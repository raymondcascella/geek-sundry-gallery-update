// ==UserScript==
// @name        Geek & Sundry Gallery Navigation
// @namespace   rcascella
// @include     http://geekandsundry.com/*
// @version     1
// @grant       none
// ==/UserScript==

var prev = $( ".gallery-wrapper a.active" ).prev();
var next = $( ".gallery-wrapper a.active" ).next();
if(prev.length > 0){
    $( "#gallery.entry-gallery h2 ~ img" ).before($('<a></a>').prop('href',prev.prop('href')).html('<< Prev'));
    $( "#gallery.entry-gallery h2 ~ img" ).after($('<a></a>').prop('href',prev.prop('href')).html('<< Prev'));
}
if(next.length > 0){
    $( "#gallery.entry-gallery h2 ~ img" ).before($('<a></a>').prop('href',next.prop('href')).html('Next >>').prop('style','float:right;'));
    $( "#gallery.entry-gallery h2 ~ img" ).after($('<a></a>').prop('href',next.prop('href')).html('Next >>').prop('style','float:right;'));
}